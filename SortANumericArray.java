//import java.util.Scanner;
import java.util.*;
public class SortANumericArray {
	public static void main(String args[]) {
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Nhap vao so phan tu cua mang: ");
		int num=keyboard.nextInt();
		int[] A = new int[num];
		int sum=0;
		for(int i=0; i< num; i++) {
			System.out.print("A["+i+"] = ");
			A[i]=keyboard.nextInt();
			sum+=A[i];
		}
		Arrays.sort(A);
		for(int i=0; i< num; i++) {
			System.out.print(" -> " + A[i]);
		}
		System.out.println("\nTong cac phan tu = " + sum);
		System.out.println("Trung binh cong = " + (double)sum/num);
	}
}