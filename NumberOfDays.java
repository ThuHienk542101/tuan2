import java.util.Scanner;
public class NumberOfDays {
	public static void main(String args[]) {
		Scanner keyboard=new Scanner(System.in);
		int num;
		
		int year;
		do {
			System.out.println("Nhap vao mot nam: ");
			year = keyboard.nextInt();
			if(year >=0)
				break;
		}while(true);
		
		String month;
		do {
			System.out.println("Nhap vao mot thang: ");
			month = keyboard.next();
			switch(month) {
				case "January": case "Jan": case "1": case "March": case "Mar": case "3": case "May":
				case "5": case "July": case "Jul": case "7": case "August": case "Aug": case "8": 
				case "October": case "Oct": case "10": case "December": case "Dec": case "12":
					System.out.println("Nam: "+ year+" thang: "+month+" co so ngay la: "+31);
					System.exit(0);
				case "February": case "Feb": case "2":
					if(year%4==0) 
						System.out.println("Nam: "+ year+" thang: "+month+" co so ngay la: "+29);
					else
						System.out.println("Nam: "+ year+" thang: "+month+" co so ngay la: "+28);
					System.exit(0);
				case "April": case "Apr": case "4": case "June": case "Jun": case "6": case "September": 
				case "Sept": case "Sep": case "9": case "November": case "Nov": case "11":
					System.out.println("Nam: "+ year+" thang: "+month+" co so ngay la: "+30);
					System.exit(0);
			}
		}while(true);
	}
}
//5.5