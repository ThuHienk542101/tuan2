//write a program to display a triangle with a height of n stars(*), n is entered by users
import javax.swing.JOptionPane;
public class DisplayATriangle {
	public static void main(String args[]) {
		String strN;
		String strNotification="You've just entered ";
		strN=JOptionPane.showInputDialog(null,"Please input n: ","Input n", JOptionPane.INFORMATION_MESSAGE);
		strNotification+=strN+"\n\n";
		double n = Double.parseDouble(strN);
		if(n<=0) {
			strNotification+="-> Invalid\n";
		}else {
			for(int i=1; i<=n; i++) {
				for(int j=i; j<=n; j++)
					strNotification+="  ";
				for(int j=1; j<=(2*i)-1;j++)
					strNotification+="* ";
				//for(int j=i; j<=n; j++)
					//strNotification+=" ";
				strNotification+="\n";
			}
		}
		JOptionPane.showMessageDialog(null, strNotification, "Show two numbers ", JOptionPane.INFORMATION_MESSAGE);
		System.exit(0);
	}
}

//5.4