import java.util.*;
public class AddTwoMatrices {
	public static void main(String args[]) {
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Co ma tran (hang*cot) = ");
		int i = keyboard.nextInt();
		int j = keyboard.nextInt();
		
		System.out.println("\nMa tran 1: ");
		int[][] A = new int[i][j];
		for(int x=0; x<i; x++){
			for(int y=0; y<j; y++) {
				System.out.print("A["+x+"]["+y+"] = ");
				A[x][y] = keyboard.nextInt();
			}
		}
		
		System.out.println("\nMa tran 2: ");
		int[][] B = new int[i][j];
		for(int x=0; x<i; x++){
			for(int y=0; y<j; y++) {
				System.out.print("B["+x+"]["+y+"] = ");
				B[x][y] = keyboard.nextInt();
				B[x][y] += A[x][y];
			}
		}
		
		System.out.println("\nTong 2 ma tran: ");
		for(int x=0; x<i; x++){
			for(int y=0; y<j; y++) {
				System.out.print("\t"+B[x][y]);
			}
			System.out.println();
		}
	}
}