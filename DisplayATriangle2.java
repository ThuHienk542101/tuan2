import java.util.Scanner;
public class DisplayATriangle2 {
	public static void main(String args[]) {
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Nhap vao n: ");
		int n = keyboard.nextInt();
		for(int i=1; i<=n; i++) {
			for(int j=i; j<=n; j++) 
				System.out.print(" ");
			for(int j=1; j<=(i*2-1); j++)
				System.out.print("*");
			System.out.println();
		}
	}
}
